# README #

This repo keeps all the files necessary to get my final year research report project running for the honours CSAM 2015 at wits.ac.za 

### What is this repository for? ###

* Source code written by me
* Makefiles with steps to download dependencies

### How do I get set up? ###

* You need to be running Ubuntu 14.04, it will not be validated against any other type of environment
* Run make while connected to the internet. It should download all dependencies automatically
* Inspect Makefile to solve any issues: Makefile is intentionally simple. And individual targets build if their deps are available

### Contribution guidelines ###

* NA

### Who do I talk to? ###

* jean@zenack.com
* djfongang@gmail.com