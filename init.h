#include "LWSSMirror.hpp"

class Init
{
public:
 
    static Init* instance() {
            static Init _init;
            return & _init;
    }
 
    void destroy();

	/// ===================================================================
	/// Access to the modules
	/// ===================================================================

	ZMQBridge* getZMQBridge() {
		return m_pZMQBridge;
	}

	LWSSMirror* getLWSSMirror() {
		return m_pLWSSMirror;
	}
 
    void startInitProcedure();
	void startBridge();
 
private:
	LWSSMirror *m_pLWSSMirror;
	ZMQBridge *m_pZMQBridge;
    /// ===================================================================
    /// Hidden methods
    /// ===================================================================
 
    /// ctor
    Init();
 
    /// copy ctor
    Init ( Init const& );
 
    /// assign op.
    Init& operator= ( Init const& );
 
    /// dtor
    ~Init() {}
};
