#ifndef JSONTHREE_HPP
#define JSONTHREE_HPP
#include <string>
#include <vector>
#include "surface.pb.h"


namespace zmqbridge {

//class Surface;
#define WS_MSG_MAX 300
#define MODEL_PART_MAX 32640
class JSONThree
{
	std::string full_value;
	zmqbridge::Surface surface;
	int current_serial;
	int32_t prev_vert_position, current_vert_position;
	int current_del_position;
public:
	JSONThree();
	~JSONThree();
	void set_surface( zmqbridge::Surface & s );
	void generate_3json( );
	void write_xyz_clouds( char * file_prefix );
	void write_xyz_cloud( char * file_prefix );
	std::string get_json_model( );
	void write_json_model( char * file_name );
	void write_to_file( const char * file_name, const std::string & str );
	std::string next_json_delta( );
	/*use
	 * std::copy(settings.vertices().begin(), settings.vertices().end(), std::front_inserter(vv));
	 * to copy a vector from any iterated sequence
	 */
};

}
#endif // JSONTHREE_HPP
