#include <iostream>
#include "ZMQBridge.hpp"
#include <sstream>
#include <zmq.hpp>
#include "rotation.pb.h"
#include "surface.pb.h"
#include "JSONThree.hpp"
#include <chrono>
#include <google/protobuf/text_format.h>

using namespace std;

ZMQBridge::ZMQBridge(int i) {
	this->testint = i;
	time_to_say_goodbye=false;

}

void ZMQBridge::stop( )
{
	time_to_say_goodbye = true;
	pooling_thr.join();
}

void ZMQBridge::init() {
	pooling_thr = std::thread( [&]()
	{
		connectToSock( "tcp://localhost:5556" );
	} );
}

std::string ZMQBridge::popInMsg()
{
	std::string res = "{}";
	if ( inbox.size() > 0 )
	{
		std::cout << "\n queue is not empty, popping out 1 of "<< inbox.size() <<" ..."<<std::endl;

		res = inbox.front();
		inbox.pop_front();
	}
	return std::move(res);
}

void ZMQBridge::pushOutMsg( std::string msg )
{
	outbox.push_back( msg );
}


std::string ZMQBridge::popOutMsg()
{
	std::string res = "{}";
	if ( outbox.size() > 0 )
	{
		std::cout << "\n queue is not empty, popping out 1 of "<< outbox.size() <<" ..."<<std::endl;

		res = outbox.front();
		outbox.pop_front();
	}
	return res;
}

void ZMQBridge::pushInMsg( std::string msg )
{
	inbox.push_back( msg );
}


bool ZMQBridge::connectToSock( const std::string & sock )
{
	zmq::context_t context ( 1 );

	//  Socket to talk to server
	std::cout << "Collecting updates from 3D Model server…\n" << std::endl;
	zmq::socket_t subscriber(context, ZMQ_SUB);
	subscriber.connect(sock);

	//  Subscribe to blank topic
	const char *filter =  "";
	subscriber.setsockopt( ZMQ_SUBSCRIBE, filter, strlen ( filter ) );

	/* FULL PARSE
	 *
	 */
	zmq::message_t request;
	//  Wait for next request from client
	subscriber.recv ( &request );
	std::cout << "Received" << std::endl;
	//zmqbridge::Rotation currAngles;
	zmqbridge::Surface latestSurface;
	std::string msg_str( static_cast<char*>( request.data() ), request.size() );
	std::cout << "incoming byte size: " << request.size() << std::endl;
	//currAngles.ParseFromString( msg_str );
	//static int x = currAngles.x() , y = currAngles.y(),  z = currAngles.z();
	latestSurface.ParseFromString( msg_str );
	std::cout << "Parsed!"  << std::endl;
	zmqbridge::JSONThree jst;
	jst.set_surface( latestSurface );
	//jst.generate_3json( );
	//jst.write_json_model( "./www/room.js" );
	jst.write_xyz_clouds( "./www/room" );
	jst.write_xyz_cloud("./www/room");

	std::string str_delta;
	do
	{
		str_delta = jst.next_json_delta();
		std::cout << "received delta of " << str_delta.size() << " bytes..." << std::endl;
		pushInMsg( str_delta );
	} while( str_delta.length() > 4 );

	return 0;
}
