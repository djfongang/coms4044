#include "JSONThree.hpp"
#include "fstream"
#include <sstream>
#include <iostream>

namespace zmqbridge {
void JSONThree::write_xyz_clouds( char * file_prefix )
{
	write_xyz_cloud( file_prefix );
	//write_to_file( ( file_prefix + std::to_string( ( int ) curr_idx / MODEL_PART_MAX  ) + ".txt" ).c_str(), ss.str() );
}

void JSONThree::write_xyz_cloud( char * file_prefix )
{
	std::stringstream ss;
	if(  surface.newpoints_size() > 2 )
	{
		auto pit = surface.newpoints().begin();
		int32_t curr_idx = 0;
		while ( pit != surface.newpoints().end() )
		{
			ss << " " << *pit;
			++pit;
			ss << " " << *pit;
			++pit;
			ss << " " << *pit << std::endl;
			++pit;
			curr_idx += 1;
		}

		write_to_file( ( std::string( file_prefix )  + ".txt" ).c_str(), ss.str() );
	}
}

void JSONThree::generate_3json( )
{
	//Building JSON surface information

	std::string head = R"!--({

					   "metadata" :
	{
			"formatVersion" : 3.1,
			"generatedBy"   : "Blender 2.65 Exporter",
			"vertices"      : )!--";

			std::string mid = R"!--(
},

"scale" : 1.000000,

"materials" : [	{
"DbgColor" : 15658734,
"DbgIndex" : 0,
"DbgName" : "default",
"vertexColors" : false
},

{
"DbgColor" : 15597568,
"DbgIndex" : 1,
"DbgName" : "Alien Skin",
"blending" : "NormalBlending",
"colorAmbient" : [0.100000011920929, 0.300000011920929, 0.100000011920929],
"colorDiffuse" : [0.500000011920929, 0.500000011920929, 0.500000011920929],
"colorSpecular" : [0.300000011920929, 0.300000011920929, 0.300000011920929],
"depthTest" : true,
"depthWrite" : true,
"shading" : "Phong",
"specularCoef" : 70,
"transparency" : 1.0,
"transparent" : false,
"vertexColors" : false
},

{
"DbgColor" : 60928,
"DbgIndex" : 2,
"DbgName" : "Alien Eyes",
"blending" : "NormalBlending",
"colorAmbient" : [0.0, 0.0, 0.0],
"colorDiffuse" : [0.0, 0.0, 0.0],
"colorSpecular" : [0.0, 0.0, 0.0],
"depthTest" : true,
"depthWrite" : true,
"shading" : "Lambert",
"specularCoef" : 50,
"transparency" : 0.7435897588729858,
"transparent" : true,
"vertexColors" : false
}], )!--";

std::stringstream ss; ss << head << surface.newpoints_size() / 3 << ",\n"
						 <<"		\"faces\"         : " << surface.addedfaces_size() / 3
						<< mid
						<< "\n \"vertices\" : [ " ;
if(  surface.newpoints_size() > 0 )
{
	auto pit = surface.newpoints().begin();
	if( pit != surface.newpoints().end() )
	{
		ss << *pit; ++pit;
	}
	while ( pit != surface.newpoints().end() )
	{
		ss << "," << *pit;
		++pit;
	}
}
ss << " ],\n";


std::cout << "Added points done!"  << std::endl;
ss << "\"faces\" : [ " ;
bool onem = false;
if( surface.addedfaces_size() > 3 )
{
	for ( size_t idx = 0; idx < surface.addedfaces_size( ); idx += 3 )
	{
		int32_t curr_t1 = ( int32_t ) surface.addedfaces( ).Get( idx );
		int32_t curr_t2 = ( int32_t ) surface.addedfaces( ).Get( idx + 1 );
		int32_t curr_t3 = ( int32_t ) surface.addedfaces( ).Get( idx + 2 );
		if( !onem )
		{
			ss << "0," << curr_t1 << "," << curr_t2 << "," << curr_t3 ; onem = true;
		}
		else
		{
			ss << ",0," << curr_t1 << "," << curr_t2 << "," << curr_t3 ;
		}
	}
}
ss << " ]\n";

ss << "}\n";
std::cout << "Added faces done!"  << std::endl;
full_value = ss.str();
}

void JSONThree::set_surface( zmqbridge::Surface & s )
{
	surface = s;
}
std::string JSONThree::get_json_model( )
{
	return full_value;
}

void JSONThree::write_json_model( char * file_name )
{
	write_to_file( file_name, full_value );
}

void JSONThree::write_to_file( const char * file_name, const std::string & str )
{
	std::ofstream out_file;
	bool caugth = false;
	try
	{
		out_file.open( file_name );
		out_file << str;
	}
	catch ( std::exception & e )
	{
		std::cout << "problem while writing to file : " << file_name << " : " << e.what() << "!"  << std::endl;
		caugth = true;
	}
	if( ! caugth )
		out_file.close( );
}
std::string JSONThree::next_json_delta( )
{

	//Building JSON surface information

	std::stringstream ss; ss << "{ \"deleted_points\" : [ " ;
	if( surface.deletedpoints_size() > current_del_position )
	{
		auto dit = surface.deletedpoints().begin();
		std::advance( dit, current_del_position );
		if ( dit != surface.deletedpoints().end() )
		{
			ss << *dit; ++dit; current_del_position++;
		}
		while ( dit != surface.deletedpoints().end() )
		{
			ss << "," << *dit;
			++dit; current_del_position++;
		}
		std::cout << "checking Deleted points done!"  << std::endl;
	}
	else
	{
		std::cout << "No Deleted points!"  << std::endl;
	}
	ss << " ],\n";


	std::cout << "adding points from "<< current_vert_position << " total size is: " << surface.newpoints_size() << std::endl;

	ss << "\"added_points\" : [ " ;
	if( surface.newpoints_size() > current_vert_position )
	{
		prev_vert_position = current_vert_position;
		int32_t u = prev_vert_position + WS_MSG_MAX;
		std::cout << "adding points from "<< current_vert_position << " to " << u << std::endl;
		auto pit = surface.newpoints().begin();
		std::advance( pit, current_vert_position );
		if( pit != surface.newpoints().end() )
		{
			ss << *pit; ++pit; current_vert_position++;
		}
		while ( pit != surface.newpoints().end() && current_vert_position < u )
		{
			ss << "," << *pit; current_vert_position++;
			++pit;
		}
		std::cout << "Added points done!"  << std::endl;
	}
	else
	{
		std::cout << "No points info!"  << std::endl;
	}
	ss << " ],\n";

	ss << "\"normals\" : [ " ;
	if( surface.normals_size() > prev_vert_position )
	{
		int32_t u = prev_vert_position + WS_MSG_MAX;
		std::cout << "adding normals from "<< prev_vert_position << " to " << u << std::endl;
		auto nit = surface.normals().begin();
		std::advance( nit, prev_vert_position );
		if( nit != surface.normals().end() )
		{
			ss << *nit; ++nit;
		}
		int i = 0;
		while ( nit != surface.normals().end() && i < u )
		{
			ss << "," << *nit; i++;
			++nit;
		}
		std::cout << "Added normals done!"  << std::endl;
	}
	else
	{
		std::cout << "No normals info!"  << std::endl;
	}
	ss << " ],\n";



	ss << "\"added_faces\" : [ " ;
	bool onem = false;
	if( surface.addedfaces_size() > 3 )
	{
		for ( size_t idx = 0; idx < surface.addedfaces_size( ); idx += 3 )
		{
			int32_t curr_t1 = ( int32_t ) surface.addedfaces( ).Get( idx );
			int32_t curr_t2 = ( int32_t ) surface.addedfaces( ).Get( idx + 1 );
			int32_t curr_t3 = ( int32_t ) surface.addedfaces( ).Get( idx + 2 );
			if( ( curr_t1 >= prev_vert_position || curr_t2 >= prev_vert_position || curr_t3 >= prev_vert_position )
					&& curr_t1 < current_vert_position && curr_t2 < current_vert_position && curr_t3 < current_vert_position )
			{
				if( !onem )
				{
					ss << curr_t1 << "," << curr_t2 << "," << curr_t3 ; onem = true;
				}
				else
				{
					ss << "," << curr_t1 << "," << curr_t2 << "," << curr_t3 ;
				}
			}
		}
		std::cout << "Added faces done!"  << std::endl;
	}
	else
	{
		std::cout << "No face info!"  << std::endl;
	}
	ss << " ],\n ";
	ss << "\"current_serial\" : " << current_serial;//<< surface.
	ss << "\n }\n";

	current_serial++;

	return ss.str();
}


JSONThree::JSONThree( )
	: current_serial( 0 ),
	  current_vert_position( 0 ),
	  prev_vert_position( 0 )
{

}

JSONThree::~JSONThree()
{

}

}
