#include <iostream>
#include "elas.h"
#include "image.h"

#include "triangle.h"

#include "zmq.hpp"
#include "surface.pb.h"
#include "rotation.pb.h"
#include <google/protobuf/text_format.h>

/*vcglib includes
 */
#include <vcg/complex/complex.h>
#include <vcg/complex/algorithms/point_sampling.h>
#include <vcg/complex/algorithms/pointcloud_normal.h>
#include <vcg/complex/algorithms/create/ball_pivoting.h>


#include <string>
#include <unistd.h>

#define WS_MSG_MAX 300
#define DISK_SAMPLING_NUM 16000

using namespace std;

/*
 * template classes for vertex cloud
 */
class MyVertex; class MyEdge; class MyFace;
struct MyUsedTypes : public vcg::UsedTypes<vcg::Use<MyVertex>   ::AsVertexType,
		vcg::Use<MyEdge>     ::AsEdgeType,
		vcg::Use<MyFace>     ::AsFaceType>{};
class MyVertex  : public vcg::Vertex< MyUsedTypes, vcg::vertex::Coord3f, vcg::vertex::Normal3f, vcg::vertex::BitFlags  >{};
class MyFace    : public vcg::Face<   MyUsedTypes, vcg::face::FFAdj,  vcg::face::VertexRef, vcg::face::BitFlags > {};
class MyEdge    : public vcg::Edge<   MyUsedTypes> {};
class MyMesh    : public vcg::tri::TriMesh< std::vector<MyVertex>, std::vector<MyFace>, std::vector<MyEdge> > {};

/*
 *   case FP_POISSONDISK_SAMPLING :
  case FP_VARIABLEDISK_SAMPLING :
  {
	if(ID(action)==FP_VARIABLEDISK_SAMPLING && !md.mm()->hasDataMask(MeshModel::MM_VERTQUALITY))
	   {
		 errorMessage = "This filter requires per vertex quality for biasing the distribution.";
		 return false; // cannot continue
	   }

	bool subsampleFlag = par.getBool("Subsample");
	if (md.mm()->cm.fn==0 && subsampleFlag==false)
	{
	  errorMessage = "This filter requires a mesh. It does not work on PointSet.";
	  return false; // cannot continue
	}

	MeshModel *curMM= md.mm();
	RenderMode rm;
	rm.drawMode = GLW::DMPoints;
	MeshModel *mm= md.addNewMesh("","Poisson-disk Samples",true,rm); // After Adding a mesh to a MeshDocument the new mesh is the current one
	mm->updateDataMask(curMM);

	float radius = par.getAbsPerc("Radius");
	int sampleNum = par.getInt("SampleNum");
	if(radius==0) radius = tri::SurfaceSampling<CMeshO,BaseSampler>::ComputePoissonDiskRadius(curMM->cm,sampleNum);
	else sampleNum = tri::SurfaceSampling<CMeshO,BaseSampler>::ComputePoissonSampleNum(curMM->cm,radius);

	Log("Computing %i Poisson Samples for an expected radius of %f",sampleNum,radius);

	// first of all generate montecarlo samples for fast lookup
	CMeshO *presampledMesh=&(curMM->cm);

	CMeshO MontecarloMesh; // this mesh is used only if we need real poisson sampling (and therefore we need to choose points different from the starting mesh vertices)

	if(!subsampleFlag)
	{
	  QTime tt;tt.start();
	  BaseSampler sampler(&MontecarloMesh);
	  sampler.qualitySampling =true;
	  tri::SurfaceSampling<CMeshO,BaseSampler>::Montecarlo(curMM->cm, sampler, sampleNum*par.getInt("MontecarloRate"));
	  MontecarloMesh.bbox = curMM->cm.bbox; // we want the same bounding box
	  presampledMesh=&MontecarloMesh;
	  Log("Generated %i Montecarlo Samples (%i msec)",MontecarloMesh.vn,tt.elapsed());
	}

	BaseSampler mps(&(mm->cm));

	tri::SurfaceSampling<CMeshO,BaseSampler>::PoissonDiskParam pp;
	tri::SurfaceSampling<CMeshO,BaseSampler>::PoissonDiskParam::Stat pds; pp.pds=&pds;

	if(ID(action)==FP_VARIABLEDISK_SAMPLING)
	{
	  pp.adaptiveRadiusFlag=true;
	  pp.radiusVariance = par.getFloat("RadiusVariance");
	  Log("Variable Density variance is %f, radius can vary from %f to %f",pp.radiusVariance,radius/pp.radiusVariance,radius*pp.radiusVariance);
	}
	else
	{
	  if(par.getBool("RefineFlag"))
	  {
		pp.preGenFlag=true;
		pp.preGenMesh=&(par.getMesh("RefineMesh")->cm);
	  }
	  pp.geodesicDistanceFlag=par.getBool("ApproximateGeodesicDistance");
	  pp.bestSampleChoiceFlag=par.getBool("BestSampleFlag");
	  pp.bestSamplePoolSize =par.getInt("BestSamplePool");
	}
	tri::SurfaceSampling<CMeshO,BaseSampler>::PoissonDiskPruning(mps, *presampledMesh, radius,pp);
	//tri::SurfaceSampling<CMeshO,BaseSampler>::PoissonDisk(curMM->cm, mps, *presampledMesh, radius,pp);
	vcg::tri::UpdateBounding<CMeshO>::Box(mm->cm);
	Point3i &g=pp.pds->gridSize;
	Log("Grid size was %i %i %i (%i allocated on %i)",g[0],g[1],g[2], pp.pds->gridCellNum, g[0]*g[1]*g[2]);
	Log("Sampling created a new mesh of %i points",md.mm()->cm.vn);
  }
	break;

*/

static void surface_from_point_cloud( const std::vector<double> &pointcloud, zmqbridge::Surface &zmqsurf )
{

	//Define the meshes
	MyMesh m, reduced_m;

	std::cout << "Processing reduced surface for " << pointcloud.size() / 3 << " Points ..." << std::endl;
	sleep( 1 );
	//Load the points into the mesh
	assert( pointcloud.size() % 3 == 0 );
	for( size_t id = 0; id < pointcloud.size(); id +=3 )
	{
		MyMesh::VertexPointer ivp[4];
		std::cout << " Adding : " << pointcloud[ id ] << ":" << pointcloud[ id + 1 ] << ":" << pointcloud[ id + 2 ] << std::endl;
		ivp[3]= &*vcg::tri::Allocator<MyMesh>::AddVertex( m, MyMesh::CoordType ( pointcloud[ id ], pointcloud[ id + 1 ], pointcloud[ id + 2 ] ) );
	}
	std::cout << "loaded mesh points ..." << std::endl;
	sleep( 1 );
	//resample to 16k vertices max
	using namespace vcg;

	std::vector<Point3f> poissonsamples;

	typedef tri::TrivialSampler<MyMesh>  BaseSampler;

//	int sampleNum = DISK_SAMPLING_NUM;
//	int radius = tri::SurfaceSampling< MyMesh, BaseSampler >::ComputePoissonDiskRadius( m, sampleNum );

//	//MyMesh *presampledMesh= &( m );

	BaseSampler mps( poissonsamples );

//	tri::SurfaceSampling< MyMesh, BaseSampler >::PoissonDiskParam pp;
//	tri::SurfaceSampling< MyMesh, BaseSampler >::PoissonDiskParam::Stat pds; pp.pds = pds;
//	//pp.adaptiveRadiusFlag=false;

//	pp.preGenFlag = false;
//	pp.preGenMesh = NULL;

//	pp.geodesicDistanceFlag = false;
//	pp.bestSampleChoiceFlag = true;
//	pp.bestSamplePoolSize = 10;

	//tri::SurfaceSampling<MyMesh,BaseSampler>::PoissonDiskPruning( mps, m, radius, pp );

	tri::SurfaceSampling<MyMesh, BaseSampler>::VertexUniform( m, mps, DISK_SAMPLING_NUM );

	vcg::tri::UpdateBounding<MyMesh>::Box( m );

	//==================
	std::cout << "resampled mesh..." << std::endl;
	sleep( 1 );
	size_t outsize = poissonsamples.size();
	for( size_t id = 0; id < outsize; ++id )
	{
		MyMesh::VertexPointer ivp[4];
		ivp[3]= &*vcg::tri::Allocator<MyMesh>::AddVertex( reduced_m, MyMesh::CoordType ( poissonsamples.at( id )[ 0 ], poissonsamples.at( id )[ 1 ], poissonsamples.at( id )[ 2 ] ) );
		std::cout << " Copying : " << poissonsamples.at( id )[ 0 ] << ":" << poissonsamples.at( id )[ 1 ] << ":" << poissonsamples.at( id )[ 2 ] << std::endl;
	}
	//==================
	std::cout << "copied resampled mesh..." << std::endl;

	//generate normals with 16 neighbors
//	vcg::tri::PointCloudNormal< MyMesh >::Param np;
//	np.fittingAdjNum = 16;
//	vcg::tri::PointCloudNormal< MyMesh >::Compute( redox_m, np );
	vcg::tri::UpdateBounding<MyMesh>::Box( reduced_m );
	vcg::tri::UpdateNormal<MyMesh>::PerVertexNormalized( reduced_m );


	tri::PointCloudNormal< MyMesh >::Param p;
	p.fittingAdjNum = 16;
	p.smoothingIterNum = 0;
	p.viewPoint = Point3f( 0, 0, 10 );
	p.useViewPoint = true;
	tri::PointCloudNormal< MyMesh >::Compute( reduced_m, p );
	//tri::UpdateBounding<CMeshO>::Box(redox_m);
	//	if( redox_m.normalmaps.size() > 0 )
	//		std::cout << "generated normals for resampled mesh!" << std::endl;
	//	else
	//		std::cout << "normals generation failed!" << std::endl;

	//generate faces ( auto settings )
	vcg::tri::BallPivoting< MyMesh > bpm( reduced_m, 0.0f, 20.0f, 90.0f );
	bpm.BuildMesh();

//	vcg::tri::UpdateBounding<MyMesh>::Box( redox_m );
//	vcg::tri::UpdateNormal<MyMesh>::PerVertexNormalized( redox_m );
	std::cout << "built surface for mesh with " << reduced_m.VN() << " vertices and " << reduced_m.face.size() << " faces !" << std::endl;

	//fill and return surface
	MyMesh::VertexIterator vi;
	MyMesh::VertexPointer  vp;
	SimpleTempData<typename MyMesh::VertContainer,int> indices(reduced_m.vert);

	std::cout << " constructed vertice indice array " << std::endl;

	size_t j;
	for( j = 0, vi = reduced_m.vert.begin(); vi != reduced_m.vert.end(); ++vi )
	{
		vp = &( *vi );
		indices[vi] = j;
		//std::cout << " allocated " << j << " to indices [vi] " << std::endl;
		if( ! vi->IsD( ) ) //    <---- Check added
		{
			zmqsurf.add_newpoints( float( vp->P()[ 0 ] ) );
			zmqsurf.add_newpoints( float( vp->P()[ 1 ] ) );
			zmqsurf.add_newpoints( float( vp->P()[ 2 ] ) );

			if ( vp->N()[0] || vp->N()[1] || vp->N()[2] )
			{
				zmqsurf.add_normals( float( vp->N()[ 0 ] ) );
				zmqsurf.add_normals( float( vp->N()[ 1 ] ) );
				zmqsurf.add_normals( float( vp->N()[ 2 ] ) );
			}
			else
			{
				std::cout << "Warning! Missing normal for current vertex!" << std::endl;
			}
			j++;
		}
	}
	std::cout << "stored vertices in zmqbridge::Surface..." << std::endl;
	MyMesh::FaceIterator fi;
	MyMesh::FacePointer fp;
	for( fi = reduced_m.face.begin(); fi != reduced_m.face.end(); ++fi )
	{
		if( ! fi->IsD( ) ) //    <---- Check added
		{
			fp = &( *fi );
			zmqsurf.add_addedfaces( indices[ fp->cV( 0 ) ] );
			zmqsurf.add_addedfaces( indices[ fp->cV( 1 ) ] );
			zmqsurf.add_addedfaces( indices[ fp->cV( 2 ) ] );
		}
	}
	std::cout << "stored faces in zmqbridge::Surface..." << std::endl;
}

int main (int argc, char** argv) {

	GOOGLE_PROTOBUF_VERIFY_VERSION;


	std::cout << "Starting stereo 3D reconstruction..." << std::endl;

	//  Prepare our context and socket
	zmq::context_t context (1);

	zmq::socket_t publisher (context, ZMQ_PUB);
	publisher.bind("tcp://*:5556");
	publisher.bind("ipc://model3d.ipc");
	std::cout << "bound to TCP port 5556 and ipc://model3d.ipc" << std::endl;


	if ( argc != 3 )

	{
		std::cout << std::endl;
		std::cout << "3D reconstruction service usage: " << std::endl;
		std::cout << "./recon3D left.pgm right.pgm \t process a single stereo pair" << std::endl;
		std::cout << "./recon3D -h \t\t shows this help" << std::endl;
		std::cout << std::endl;
		std::cout << "Note: All images must be pgm greylevel images. All output" << std::endl;
		std::cout << "      disparities will be scaled such that disp_max = 255." << std::endl;
		std::cout << std::endl;
		return EXIT_FAILURE;
	}

	//process( argv[1], argv[2], D1, D2 );
	// load images
	image<uchar> *I1,*I2;
	I1 = loadPGM(argv[1]);
	I2 = loadPGM(argv[2]);

	// check for correct size
	if (I1->width()<=0 || I1->height() <=0 || I2->width()<=0 || I2->height() <=0 ||
			I1->width()!=I2->width() || I1->height()!=I2->height()) {
		std::cout << "ERROR: Images must be of same size, but" << std::endl;
		std::cout << "       I1: " << I1->width() <<  " x " << I1->height() <<
					 ", I2: " << I2->width() <<  " x " << I2->height() << std::endl;
		delete I1;
		delete I2;
		return EXIT_FAILURE;
	}

	// get image width and height
	int32_t width  = I1->width();
	int32_t height = I1->height();

	// allocate memory for disparity images
	const int32_t dims[3] = {width,height,width}; // bytes per line = width
	float *D1_data = (float*)malloc(width*height*sizeof(float));
	float *D2_data = (float*)malloc(width*height*sizeof(float));

	// process
	Elas::parameters param;
	param.postprocess_only_left = false;
	Elas elas(param);
	elas.process(I1->data,I2->data,D1_data,D2_data,dims);

	// find maximum disparity for scaling output disparity images to [0..255]
	float disp_max = 0;
	for (int32_t i=0; i<width*height; i++) {
		if (D1_data[i]>disp_max) disp_max = D1_data[i];
		if (D2_data[i]>disp_max) disp_max = D2_data[i];
	}

	std::cout << "Stereo 3D reconstruction completed !" << std::endl;

	std::cout << "width is:" << width << std::endl;

	std::cout << "height is:" << height << std::endl;

	int d_size = height * width;

	std::cout << "New Surface event processing Completed! " << std::endl;

	std::cout << "Surface delta processing for " << d_size << " points starting..." << std::endl;

	int z = 2;
	while ( z > 1 ) {
		z--;
		zmqbridge::Surface surf;
		int serial_delta = 0;

		//Use all points and depths for surface recon
		std::vector<double> coords_block;
		size_t x_i = 0, y_i = 0, x_step = width / 3, y_step = height / 3;
		coords_block.reserve( x_step * y_step * 3 );
		while ( ( x_i + 1 ) * x_step < width )//process in blocks of 1/3*1/3 of source images
		{
			y_i = 0;
			while( y_i * y_step < height )
			{
				for( size_t i = 0; i < x_step; ++i )
				{
					for( size_t j = 0; j < y_step; ++j )
					{
						//add the point to part surface's xyz
						size_t _x = x_i * x_step + i;
						size_t _y = y_i * y_step + j;
						size_t _p = _y * width + _x;
						//std::cout << "x_i: " << x_i << ", y_i: " << y_i << ", i: " << i << ", j: " << j << ", _x: " << _x << ", _y: " << _y << ", _p: " << _p << "..." << std::endl;
						uint8_t tmp_depth = (uint8_t)std::max(255.0*D1_data[ _p ]/disp_max,0.0);
						if( tmp_depth > 0 )//check that we have an actual depth here. also need to merge points on same lines
						{
							coords_block.push_back( 20.0*_x/width - 10.0 );
							coords_block.push_back( 20.0*_y/height - 10.0 );
							coords_block.push_back( 5.0*tmp_depth/255.0 - 2.5 );
						}
					}
				}

				//process part surface's xyz and send surface to bridge
				surface_from_point_cloud( coords_block, surf );
				coords_block.clear();
				std::string msg_str;
				surf.set_serial( serial_delta );
				surf.SerializeToString( &msg_str );

				// create a zmq message from the serialized string
				zmq::message_t request (msg_str.size());
				memcpy ((void *) request.data (), msg_str.c_str(), msg_str.size());
//				std::cout << "Sending Surface delta number: " << serial_delta << "..." << std::endl;
//								std::string text_str;
//								google::protobuf::TextFormat::PrintToString( surf, &text_str );
//								std::cout << "Value: " << text_str << "..." << std::endl;
				publisher.send ( request );
				zmqbridge::Surface ns;
				surf = ns;
				serial_delta++;
				y_i++;
			}
			x_i++;
			std::cout << "x_i * x_step: " <<  x_i * x_step << "..." << std::endl;
		}
		sleep( 5 );

	}

	return 0;
}
