
#include <unistd.h>
#include <signal.h>

#include "init.h"

static int force_exit = 0;

Init* init;

void sighandler( int sig )
{
	force_exit = 1;

	init->instance()->destroy( );
}

int main( int argc, char **argv )
{
	signal( SIGINT, sighandler );

	init->instance()->startBridge( );


	return 0;
}

