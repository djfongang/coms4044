#ifndef LWSSMIRROR_HPP
#define LWSSMIRROR_HPP
#include <string>
#include "ZMQBridge.hpp"

class LWSSMirror
{
public:
	LWSSMirror();
	~LWSSMirror();
	void init(int argc, char **argv);
	void loop();
	void stop();
	void exit();

	bool isRunning() { return m_isRunning; }


	void setDataAvailable() { m_updateAvailable = true; }

private:

	void sendUpdate();

	void updateBuffer(std::string& payloadString);

private:

	int force_exit;
	bool m_isRunning;

	struct libwebsocket_context * m_pContext;

	bool m_updateAvailable;
};

#endif // LWSSMIRROR_HPP
