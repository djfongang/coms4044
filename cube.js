{

    "metadata" :
    {
        "formatVersion" : 3.1,
        "generatedBy"   : "Blender 2.65 Exporter",
        "vertices"      : 8,
        "faces"         : 12
    },

    "scale" : 1.000000,

    "materials" : [	{
        "DbgColor" : 15658734,
        "DbgIndex" : 0,
        "DbgName" : "default",
        "vertexColors" : false
    },

    {
        "DbgColor" : 15597568,
        "DbgIndex" : 1,
        "DbgName" : "Alien Skin",
        "blending" : "NormalBlending",
        "colorAmbient" : [0.100000011920929, 0.300000011920929, 0.100000011920929],
        "colorDiffuse" : [0.500000011920929, 0.500000011920929, 0.500000011920929],
        "colorSpecular" : [0.300000011920929, 0.300000011920929, 0.300000011920929],
        "depthTest" : true,
        "depthWrite" : true,
        "shading" : "Phong",
        "specularCoef" : 70,
        "transparency" : 1.0,
        "transparent" : false,
        "vertexColors" : false
    },

    {
        "DbgColor" : 60928,
        "DbgIndex" : 2,
        "DbgName" : "Alien Eyes",
        "blending" : "NormalBlending",
        "colorAmbient" : [0.0, 0.0, 0.0],
        "colorDiffuse" : [0.0, 0.0, 0.0],
        "colorSpecular" : [0.0, 0.0, 0.0],
        "depthTest" : true,
        "depthWrite" : true,
        "shading" : "Lambert",
        "specularCoef" : 50,
        "transparency" : 0.7435897588729858,
        "transparent" : true,
        "vertexColors" : false
    }],

    "vertices" : [ 10,10,10, 10,-10,10, 10,-10,-10, 10,10,-10,  -10,10,10, -10,-10,10, -10,-10,-10, -10,10,-10 ],

    "faces" : [ 0, 0,1,2, 0, 0,2,3, 0, 0,3,7, 0, 0,7,4, 0, 0,4,5, 0, 0,5,1, 0, 6,7,4, 0, 6,4,5, 0, 6,7,3, 0, 6,3,2, 0, 6,5,1, 0, 6,1,2 ]

}
