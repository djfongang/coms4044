#include "zmq.hpp"
#include "surface.pb.h"
#include "rotation.pb.h"

//#include "del_interface.hpp"

#include "elas.h"
#include "image.h"
#include <string>
#include <iostream>
#include <unistd.h>

#define WS_MSG_MAX 1400
//void compute_triangles( int32_t *coords_x, int32_t *coords_y, int32_t coords_len, std::vector<int32_t> *triangles );
//void compute_triangles( int32_t *coords_x, int32_t *coords_y, int32_t coords_len, std::vector<int32_t> *triangles )
//{
//	// input/output structure for triangulation
//	struct triangulateio in, out;
//	int32_t k;

//	// inputs
//	in.numberofpoints = coords_len;
//	in.pointlist = ( float* ) malloc( in.numberofpoints * 2 * sizeof( float ) );
//	k=0;

//	for ( int32_t i = 0; i < coords_len; i++ )
//	{
//		in.pointlist[ k++ ] = coords_x[ i ];
//		in.pointlist[ k++ ] = coords_y[ i ];
//	}

//	in.numberofpointattributes = 0;
//	in.pointattributelist      = NULL;
//	in.pointmarkerlist         = NULL;
//	in.numberofsegments        = 0;
//	in.numberofholes           = 0;
//	in.numberofregions         = 0;
//	in.regionlist              = NULL;

//	// outputs
//	out.pointlist              = NULL;
//	out.pointattributelist     = NULL;
//	out.pointmarkerlist        = NULL;
//	out.trianglelist           = NULL;
//	out.triangleattributelist  = NULL;
//	out.neighborlist           = NULL;
//	out.segmentlist            = NULL;
//	out.segmentmarkerlist      = NULL;
//	out.edgelist               = NULL;
//	out.edgemarkerlist         = NULL;

//	// do triangulation (z=zero-based, n=neighbors, Q=quiet, B=no boundary markers)
//	char parameters[] = "zQB";

//	triangulate( parameters, &in, &out, NULL );

//	// put resulting triangles into vector tri
//	k=0;
//	for ( int32_t i = 0; i < out.numberoftriangles; i++ )
//	{
//		triangles->push_back( out.trianglelist[ k ] );
//		triangles->push_back( out.trianglelist[ k+1 ] );
//		triangles->push_back( out.trianglelist[ k+2 ] );
//		k += 3;
//	}
//}

using namespace tpp;

int main( int argc, char** argv )
{
	//TODO: split code out of main into logical subsystem that can have competing implementations (but with zero copy):
	//stereoCam process to publish imagePairUpdateEvent(s) using local ipc that contain pairs of images  with corresponding camera positions
	/*
 depth_information_event
	current_center_front_look_at( pitch, roll, yaw ) [synthetic stereo view]
	depth_map( <=> grayscale_image )

make sure cameras generate very low cost depth info events very often and modify probabilities of 3D points being on a surface accordingly.
investigate de possiblity of using different information at different moments to handle it.
other idea: use priors on points that can be seen from previous estimates
- bright idea: object marker shaped like detected object and then unsupervised learned
- in machine learning implementation, the feature vector is just the part of current window (max visible space=~ 1920*1200) that has very low accuracy.
- requires a very precise IMU
   */



	//std::cout << "Verifiyng version of google protocol buffers..." << std::endl;
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	//std::cout << "Version verified !" << std::endl;
	//	zmqbridge::Rotation r;
	//	r.set_x( 1 );
	//	r.set_y( 1 );
	//	r.set_z( 1 );
	//std::cout << "values set, creating context..." << std::endl;

	//create data structures to use to receive the disparity map.

	std::cout << "Starting stereo 3D reconstruction..." << std::endl;

	//  Prepare our context and socket
	zmq::context_t context (1);

	zmq::socket_t publisher (context, ZMQ_PUB);
	publisher.bind("tcp://*:5556");
	publisher.bind("ipc://model3d.ipc");
	std::cout << "bound to TCP port 5556 and ipc://model3d.ipc" << std::endl;

	if ( argc != 3 )

	{
		std::cout << std::endl;
		std::cout << "3D reconstruction service usage: " << std::endl;
		std::cout << "./srv3D left.pgm right.pgm \t process a single stereo pair" << std::endl;
		std::cout << "./srv3D -h \t\t shows this help" << std::endl;
		std::cout << std::endl;
		std::cout << "Note: All images must be pgm greylevel images. All output" << std::endl;
		std::cout << "      disparities will be scaled such that disp_max = 255." << std::endl;
		std::cout << std::endl;
		return EXIT_FAILURE;
	}

	//process( argv[1], argv[2], D1, D2 );
	// load images
	image<uchar> *I1,*I2;
	I1 = loadPGM(argv[1]);
	I2 = loadPGM(argv[2]);

	// check for correct size
	if (I1->width()<=0 || I1->height() <=0 || I2->width()<=0 || I2->height() <=0 ||
			I1->width()!=I2->width() || I1->height()!=I2->height()) {
		std::cout << "ERROR: Images must be of same size, but" << std::endl;
		std::cout << "       I1: " << I1->width() <<  " x " << I1->height() <<
					 ", I2: " << I2->width() <<  " x " << I2->height() << std::endl;
		delete I1;
		delete I2;
		return EXIT_FAILURE;
	}

	// get image width and height
	int32_t width  = I1->width();
	int32_t height = I1->height();

	// allocate memory for disparity images
	const int32_t dims[3] = {width,height,width}; // bytes per line = width
	float *D1_data = (float*)malloc(width*height*sizeof(float));
	float *D2_data = (float*)malloc(width*height*sizeof(float));

	// process
	Elas::parameters param;
	param.postprocess_only_left = false;
	Elas elas(param);
	elas.process(I1->data,I2->data,D1_data,D2_data,dims);

	// find maximum disparity for scaling output disparity images to [0..255]
	float disp_max = 0;
	for (int32_t i=0; i<width*height; i++) {
		if (D1_data[i]>disp_max) disp_max = D1_data[i];
		if (D2_data[i]>disp_max) disp_max = D2_data[i];
	}

	std::cout << "Stereo 3D reconstruction completed !" << std::endl;

	std::cout << "width is:" << width << std::endl;

	std::cout << "height is:" << height << std::endl;

	int d_size = height * width;

	std::cout << "New Surface event processing Completed! " << std::endl;

	std::cout << "New Surface delta of " << d_size << " points processing starting..." << std::endl;
	//	TODO:insert internal model processing here
	/*Reprojection to 3D can be performed via

X = (u-cu)*base/d
Y = (v-cv)*base/d
Z = f*base/d
*/
	//For disparity to depth calculation, simply invert and normalize since focal distance (in pixels) and baseline (in meters) are constant.
	//TODO: triangulate and create transfer list.

	//split list in parts. Turn off sleeping while transferring each full list, then sleep 5 seconds between transfers.
	//Keep vertex numbering between updates, so that we can simply replace corresponding triangles with new ones.
	using std::cout;
	while (true) {
		zmqbridge::Surface s;
		int serial_delta = 0;

//		int32_t number_of_points = width * height;
//		int32_t* coords_x = ( int32_t* ) malloc( number_of_points * sizeof( int32_t ) );
//		int32_t* coords_y = ( int32_t* ) malloc( number_of_points * sizeof( int32_t ) );

		std::vector< Delaunay::Point > pointsIn;
		//number_of_points = 0;
		for ( size_t curr_x = 0; curr_x < width ; ++curr_x )
		{
			for ( size_t curr_y = 0; curr_y < height ; ++curr_y )
			{
				uint8_t tmp_depth = (uint8_t)std::max(255.0*D1_data[ curr_x * width + curr_y ]/disp_max,0.0);//this is the disparity at that point, need to set the depth instead
				if( tmp_depth > 0 )//check that we have an actual depth here. also need to merge points on same lines
				{
//					coords_x[ number_of_points ] = curr_x;
//					coords_y[ number_of_points ] = curr_y;
//					number_of_points++;
					Delaunay::Point tempP( curr_x, curr_y );
					pointsIn.push_back( tempP );
				}
			}
		}

		Delaunay our_triangles( pointsIn );
		our_triangles.Triangulate();

		std::cout <<"======================================\n";
		std::cout <<"     Faces from face iterator         \n";
		std::cout <<"======================================\n";
		for(Delaunay::fIterator fit  = our_triangles.fbegin();
								fit != our_triangles.fend();
							  ++fit){
			std::cout << our_triangles.Org(fit)  << ", "
				 << our_triangles.Dest(fit) << ", "
				 << our_triangles.Apex(fit) << " \t: Area = "
				 << our_triangles.area(fit) << std::endl;

			std::cout << "Adjacent vertices are: ";
			for( int i =0; i < 3; ++i )
				std::cout << our_triangles.Sym( fit, i ) << "\t";
			std::cout << std::endl;
		}

//		std::vector<int32_t> *triangles = new std::vector<int32_t>( );
//		compute_triangles( coords_x, coords_y, number_of_points, triangles );

		//		for ( size_t curr_x = 0; curr_x < width ; ++curr_x )
		//		{
		//			for ( size_t curr_y = 0; curr_y < height ; ++curr_y )
		//			{
		//				uint8_t tmp_depth = (uint8_t)std::max(255.0*D1_data[ curr_x * width + curr_y ]/disp_max,0.0);//this is the disparity at that point, need to set the depth instead
		//				if( tmp_depth > 0 )//check that we have an actual depth here. also need to merge points on same lines
		//				{
		//					s.add_newpoints( curr_x );
		//					s.add_newpoints( curr_y );
		//					s.add_newpoints( tmp_depth );
		//				}

		//				if( s.newpoints_size() >  WS_MSG_MAX )
		//				{
		//					std::string msg_str;

		//					s.SerializeToString( &msg_str );

		//					// create a zmq message from the serialized string
		//					zmq::message_t request (msg_str.size());
		//					memcpy ((void *) request.data (), msg_str.c_str(), msg_str.size());
		//					std::cout << "Sending Surface delta number: " << serial_delta << "..." << std::endl;
		//					publisher.send ( request );
		//					zmqbridge::Surface n;
		//					s = n;
		//					serial_delta++;
		//				}
		//			}
		//		}

		//match the point numbers inside triangle to their coordinates in x and y
		//add full triangles sequentially
//		for( auto it = triangles->begin(); it != triangles->end(); ++it )
//		{
//			int32_t curr_1 = *it;
////			it++;
////			int32_t curr_2 = it;
////			it++;
////			int32_t curr_3 = it;

//			uint8_t tmp_depth = (uint8_t)std::max(255.0*D1_data[ curr_1 ]/disp_max,0.0);
//			s.add_newpoints( coords_x[ curr_1 ] );
//			s.add_newpoints( coords_y[ curr_1 ] );
//			s.add_newpoints( tmp_depth );
//		}
		serial_delta++;
		sleep( 5 );

	}

	// free memory
	delete I1;
	delete I2;

	free(D1_data);
	free(D2_data);

	// Optional:  Delete all global objects allocated by libprotobuf.
	google::protobuf::ShutdownProtobufLibrary();
	return EXIT_SUCCESS;
}
