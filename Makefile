CFLAGS=-Wall -Werror -g -ansi -pedantic -std=c89
CCFLAGS=-Wall -Werror -g
CPPFLAGS=  -std=c++11 -fPIC
LDFLAGS=-g -Wall -lstdc++
DLDFLAGS=-shared

module1_name = zeromq
version1 = 3.2.5
module1 = $(module1_name)-$(version1)
file1 = $(module1).tar.gz
url1 = http://download.zeromq.org/$(file1)

module2_name = protobuf
version2 = 2.6.1
module2 = $(module2_name)-$(version2)
file2 = $(module2).tar.gz
url2 = https://github.com/google/protobuf/releases/download/v$(version2)/$(file2)

module3_name = libwebsockets
version3 = 1.4-chrome43-firefox-36
module3 = $(module3_name)-$(version3)
file3 = v$(version3).tar.gz
url3 = https://github.com/warmcat/$(module3_name)/archive/$(file3)

file4 = three.min.js
url4 = http://threejs.org/build/$(file4)

file5 = libelas.zip
url5 = http://www.cvlibs.net/downloads/$(file5)

BRIDGE_DIR=$(CURDIR)

TYPES=rotation.pb.o surface.pb.o
LDOBJS=$(TYPES) JSONThree.o ZMQBridge.o LWSSMirror.o init.o
CLIOBJ=netclient.o


TRIANGLE_FLAGS= -DREDUCED -DANSI_DECLARATORS -DTRILIBRARY -DCDT_ONLY -DLINUX
TRIANGLE=$(BRIDGE_DIR)/elas/triangle.cpp.o

APP_LFLAGS= -L$(ZMQBRIDGE_LIBDIR)

ZMQBRIDGE_INCDIR=$(BRIDGE_DIR)/include
ZMQBRIDGE_LIBDIR=$(BRIDGE_DIR)/lib
ZMQBRIDGE_BINDIR=$(BRIDGE_DIR)/bin

CPPFLAGS+= -I$(ZMQBRIDGE_INCDIR)
LDFLAGS+= -L$(ZMQBRIDGE_LIBDIR)
LFLAGS= -lzmq -lprotobuf -lwebsockets

ZMQ_TO_WS=$(ZMQBRIDGE_LIBDIR)/libzmqbridge.so
CLI=model3D

SRV=$(ZMQBRIDGE_BINDIR)/libwebsockets-test-server
WWW_FILES=$(BRIDGE_DIR)/test.html $(BRIDGE_DIR)/cube.js

#external deps
MODULE1_AR=../deps/$(file1)
MODULE2_AR=../deps/$(file2)
MODULE3_AR=../deps/$(file3)

ELAS_AR=../deps/$(file5)
ELAS=$(ZMQBRIDGE_LIBDIR)/liblase.so $(ZMQBRIDGE_INCDIR)/elas.h $(ZMQBRIDGE_INCDIR)/triangle.h

MODULE1=$(ZMQBRIDGE_INCDIR)/zmq.hpp $(ZMQBRIDGE_INCDIR)/zmq.h $(ZMQBRIDGE_LIBDIR)/libzmq.so
MODULE2=$(ZMQBRIDGE_INCDIR)/google $(ZMQBRIDGE_LIBDIR)/libprotobuf.so
MODULE3=$(ZMQBRIDGE_INCDIR)/libwebsockets.h $(ZMQBRIDGE_LIBDIR)/libwebsockets.so ../deps/$(module3)

BRSRV=$(ZMQBRIDGE_BINDIR)/front3D


THREE_JS=../deps/$(file4) ../deps/pnltri.min.js

ELAS_SRC_DIR=/export/Wits/COMS4044_ResearchProjectComputerScience/deps/libelas/src
ELAS_CFLAGS= -msse3 -O3 -DNDEBUG
#ELAS_CFLAGS=-O3 -DNDEBUG
ELAS_CXX=/usr/bin/c++
ELAS_OBJ=$(ELAS_SRC_DIR)/filter.cpp.o $(ELAS_SRC_DIR)/triangle.cpp.o $(ELAS_SRC_DIR)/elas.cpp.o $(ELAS_SRC_DIR)/descriptor.cpp.o $(ELAS_SRC_DIR)/matrix.cpp.o
ELAS_IFLAGS= -I$(ELAS_SRC_DIR)/

RECON=$(BRIDGE_DIR)/bin/recon3D
#all: $(MODULE1) $(MODULE2) $(MODULE3)
all: $(BRSRV) $(ELAS) $(RECON) $(WWW_FILES)
#all: $(ELAS) $(ELAS_OBJ) $(CLI)
default:all

netclient.o:
	$(CXX) $(ELAS_CFLAGS) $(TRIANGLE_FLAGS) $(CPPFLAGS) -o $(BRIDGE_DIR)/netclient.o -c $(BRIDGE_DIR)/netclient.cpp

%.pb.cc: %.proto
	./bin/protoc --cpp_out=. $<
	./bin/protoc --python_out=. $<
	cp *.pb.h $(ZMQBRIDGE_INCDIR)/

%.cpp.o : %.cpp $(ELAS)
	$(ELAS_CXX) $(ELAS_CFLAGS) -I$(BRIDGE_DIR)/vcglib -I$(ZMQBRIDGE_INCDIR) $(ELAS_IFLAGS) -o $@ -c $<

%.pb.o : %.pb.cc
		$(CXX) $(CPPFLAGS) -c -o $@ $<

%.o: %.cc
	$(CC) $(CCFLAGS) -c $<

%.o: %.c
	$(CC) $(CFLAGS) -c $<

$(THREE_JS):
	 cd ../deps/
	 wget -c $(url4); wget -c https://raw.github.com/jahting/pnltri.js/master/build/pnltri.min.js

$(RECON): $(ELAS_OBJ) $(BRIDGE_DIR)/Recon3D.cpp.o
	$(ELAS_CXX) $(ELAS_CFLAGS) $(ELAS_OBJ) $(BRIDGE_DIR)/Recon3D.cpp.o $(TYPES) -L$(ZMQBRIDGE_LIBDIR) -lzmq -lprotobuf -o $@ -rdynamic

$(MODULE1):$(MODULE1_AR)
	 cd ../deps/; tar -zxvpf $(file1); cd $(module1); ./configure --prefix=$(BRIDGE_DIR); make; make install; \
	 wget -c https://raw.githubusercontent.com/zeromq/cppzmq/master/zmq.hpp -O $(BRIDGE_DIR)/include/zmq.hpp

$(EXT_DEPS):$(MODULE1) $(MODULE2) $(MODULE3)

$(MODULE1_AR):
	 cd ../deps/; wget -c $(url1)

$(MODULE2):$(MODULE2_AR)
	 cd ../deps/; tar -zxvpf $(file2); cd $(module2); ./configure --prefix=$(BRIDGE_DIR); make; make install

$(MODULE2_AR):
	 cd ../deps/; wget -c $(url2)

$(ELAS):$(ELAS_AR)
	 cd ../deps/;  rm -rf libelas; mkdir libelas;cd libelas;unzip ../libelas.zip; \
	 mkdir -p $(BRIDGE_DIR)/elas/; \
	 cp src/* $(BRIDGE_DIR)/elas/


$(ELAS_AR):
	 cd ../deps/; wget -c $(url5)

#NOTE:  -DCMAKE_INSTALL_PREFIX:PATH=
$(MODULE3):$(MODULE3_AR)
	 cd ../deps/; tar -zxvpf $(file3); cd $(module3); export DESTDIR=$(BRIDGE_DIR); cmake .; make; make install; \
	 cp $(BRIDGE_DIR)/usr/local/include/*.h $(ZMQBRIDGE_INCDIR)/; \
	 cp $(BRIDGE_DIR)/usr/local/lib/*.so $(ZMQBRIDGE_LIBDIR)/


$(MODULE3_AR):
	 cd ../deps/; wget -c $(url3)

$(CLI): $(CLIOBJ) $(ELAS_OBJ)
	 cp /export/Wits/COMS4044_ResearchProjectComputerScience/piyush/piyush-triangle-8e084aebc825/lib/libdel.a /export/Wits/COMS4044_ResearchProjectComputerScience/coms4044/lib/
	 cp /export/Wits/COMS4044_ResearchProjectComputerScience/piyush/piyush-triangle-8e084aebc825/include/del_interface.hpp /export/Wits/COMS4044_ResearchProjectComputerScience/coms4044/include/
	 #$(CXX) -std=c++11  $(TRIANGLE_FLAGS)  $(ELAS_IFLAGS)  $(ELAS_OBJ) $(CLIOBJ) $(TYPES) $(APP_LFLAGS) -L./ $(ELAS_CFLAGS) -ldel $(LFLAGS) -o $@ -rdynamic
	 $(ELAS_CXX) -std=c++11 $(ELAS_CFLAGS) $(ELAS_OBJ) $(CLIOBJ) $(TYPES) $(APP_LFLAGS) -o $@ -rdynamic

$(BRSRV): $(ZMQ_TO_WS) $(MODULE3)
	 $(CXX) -ggdb main.cpp $(CPPFLAGS) $(CCFLAGS) $(APP_LFLAGS) $(LFLAGS) -lzmqbridge -o $@

$(ZMQ_TO_WS): $(EXT_DEPS) $(LDOBJS)
	 $(CC) $(LDOBJS) $(LDFLAGS) $(DLDFLAGS) $(LFLAGS) -o $@
	 cp -rf $(ZMQBRIDGE_INCDIR)/google $(ZMQ_TO_WS) $(ZMQBRIDGE_LIBDIR)/libzmq.* $(ZMQBRIDGE_LIBDIR)/libprotobuf.* ../deps/$(module3)/lib/
clean:
	rm -rf $(ZMQ_TO_WS) ../deps/libelas $(BRIDGE_DIR)/elas
	rm -f *.o netcli
	rm -rf ../deps/$(module3)/lib/google
	#cp /export/Wits/COMS4044_ResearchProjectComputerScience/piyush/piyush-triangle-8e084aebc825/lib/libdel.a /export/Wits/COMS4044_ResearchProjectComputerScience/coms4044/lib/
	#cp /export/Wits/COMS4044_ResearchProjectComputerScience/piyush/piyush-triangle-8e084aebc825/include/*.hpp /export/Wits/COMS4044_ResearchProjectComputerScience/coms4044/include/
	#cp /export/Wits/COMS4044_ResearchProjectComputerScience/piyush/piyush-triangle-8e084aebc825/include/triangle.h /export/Wits/COMS4044_ResearchProjectComputerScience/coms4044/include/

test:
	@echo $(CURDIR)
	@echo $(notdir $(CURDIR))
$(WWW_FILES):$(THREE_JS)
	cp -v $(BRIDGE_DIR)/ext/*.html $(BRIDGE_DIR)/www/
	cp -v $(BRIDGE_DIR)/cube.js $(BRIDGE_DIR)/www/
	cp -v $(BRIDGE_DIR)/ext/favicon.ico $(BRIDGE_DIR)/www/
	cp -v $(THREE_JS) $(BRIDGE_DIR)/www/

