#include <deque>
#include <string>
#include <thread>

#define ZMQ_POOL_INTERVAL 20

class ZMQBridge {
        public:
				void init();
                ZMQBridge(int i);
				std::string  popInMsg();
				void pushOutMsg( std::string msg );
				std::string  popOutMsg();
				void pushInMsg( std::string  msg );
				void stop( );
				bool connectToSock( const std::string & sock = "tcp://*:555" );

        private:
                int testint;
				std::deque<std::string> inbox;
				std::deque<std::string> outbox;
				bool time_to_say_goodbye;
				std::thread pooling_thr;
};
