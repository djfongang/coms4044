#include "init.h"

#include <iostream>

Init::Init( )
{
	m_pZMQBridge = NULL;
	m_pLWSSMirror = NULL;
}

void Init::destroy( )
{
	m_pLWSSMirror->stop( );
}

void Init::startBridge( )
{
	m_pZMQBridge = new ZMQBridge( 5 );
	m_pZMQBridge->init( );
 
	std::cout << "bridge initialized!\n" << std::endl;
	m_pLWSSMirror = new LWSSMirror( );
	std::cout << "mirror created!\n" << std::endl;
	m_pLWSSMirror->init( 0, NULL );
	std::cout << "mirror initialized!\n" << std::endl;
	m_pLWSSMirror->loop( );
 
    /// wait
	while ( m_pLWSSMirror->isRunning( ) ) {}
	m_pLWSSMirror->exit( );
	delete m_pLWSSMirror;
}
